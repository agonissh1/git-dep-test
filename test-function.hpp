#ifndef TEST_FUNCTION_HPP
#define TEST_FUNCTION_HPP

#include <iostream>
#include <string>


std::string testGitlabDependancyString(){
    return "Test gitlab fetching dependencies";
}

std::string anotherFunction(){
    return "Checking if last commit";
}

std::string preLastCommitTest(){
    return "The V3 of the code here :)";
}

std::string lastCommitTest(){
    return "V4 here";
}

#endif
